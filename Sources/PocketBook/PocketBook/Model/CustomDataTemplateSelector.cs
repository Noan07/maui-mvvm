﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PocketBook.Model
{
    public class CustomDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate DefaultTemplate { get; set; }
        public DataTemplate LastItemTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var itemsView = (CollectionView)container;

            if (item == itemsView.ItemsSource.Cast<object>().LastOrDefault())
            {
                return LastItemTemplate;
            }
            else
            {
                return DefaultTemplate;
            }
        }
    }
}
