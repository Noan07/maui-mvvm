﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PocketBook.Model
{
    public class ActionItem
    {
        public string IconItem { get; set; }
        public string NameItem { get; set; }
    }
}
