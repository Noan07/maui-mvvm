﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PocketBook.Model
{
    public class LibraryItem
    {
        public string NbItem { get; set; }
        public string IconItem { get; set; }
        public string NameItem { get; set; }

        public ContentPage PageName { get; set; }
    }
}
