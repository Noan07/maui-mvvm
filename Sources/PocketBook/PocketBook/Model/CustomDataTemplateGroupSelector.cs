﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace PocketBook.Model
{
    class CustomDataTemplateGroupSelector : DataTemplateSelector
    {
        public DataTemplate DefaultTemplate { get; set; }
        public DataTemplate LastItemTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var collectionView = (CollectionView)container;
            var group = collectionView.ItemsSource as IEnumerable<IGrouping<string, BookItem>>;

            foreach (var groupItem in group)
            {
                if (groupItem.Last() == item)
                {
                    return LastItemTemplate;
                }
            }

            return DefaultTemplate;
        }
    }
}
