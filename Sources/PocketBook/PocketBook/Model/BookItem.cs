﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PocketBook.Model
{
    public class BookItem
    {
        public string Title { get; set; }
        public string Author { get; set; }

        public string Image { get; set; }
        public int Grade { get; set; }

        public string WebSite { get; set; }
        public string Edition { get; set; }

        public string ISBN { get; set; }

        public string Langue { get; set; }

        public int NbPages { get; set; }

        public int NbItems { get; set; }

        public string Summary { get; set; }

        public string Status { get; set; }
        public int Year { get; set; }


    }

}
