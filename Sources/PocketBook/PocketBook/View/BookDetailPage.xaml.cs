using PocketBook.Model;
using System.Collections.ObjectModel;

namespace PocketBook.View;

public partial class BookDetailPage : ContentPage
{
    public ObservableCollection<ActionItem> ListItems { get; set; }
    public BookDetailPage()
	{
		InitializeComponent();



        ListItems = new ObservableCollection<ActionItem>
        {
            new ActionItem {  IconItem = "folder", NameItem = "D�placer le livre"},
            new ActionItem {  IconItem = "add", NameItem = "Ajouter � la liste � lire plus tard"},
            new ActionItem {  IconItem = "eyeglasses", NameItem = "Changer le statut de lecture"},
            new ActionItem {  IconItem = "person", NameItem = "Pr�ter le livre"}
        };

        ActionsListView.ItemsSource = ListItems;
    }
    private async void OnCustomBackButtonClicked(object sender, EventArgs e)
    {
        await Navigation.PopAsync();
    }

}