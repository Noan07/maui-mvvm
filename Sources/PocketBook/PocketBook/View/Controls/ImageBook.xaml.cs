namespace PocketBook.View.Controls;

public partial class ImageBook : ContentView
{

    public static readonly BindableProperty SourceImageProperty = BindableProperty.Create(
    nameof(SourceImage),
    typeof(string),
    typeof(ImageBook), "jjk");

    public string SourceImage
    {
        get => (string)GetValue(SourceImageProperty);
        set => SetValue(SourceImageProperty, value);

    }

    public static readonly BindableProperty SizeImageProperty = BindableProperty.Create(
    nameof(SizeImage),
    typeof(int),
    typeof(ImageBook), 0);

    public int SizeImage
    {
        get => (int)GetValue(SizeImageProperty);
        set => SetValue(SizeImageProperty, value);

    }
    public ImageBook()
	{
		InitializeComponent();
	}
}