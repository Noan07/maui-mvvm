namespace PocketBook.View.Controls;

public partial class ItemMenu : ContentView
{
    public static readonly BindableProperty IconItemProperty = BindableProperty.Create(nameof(IconItem), typeof(string), typeof(ItemMenu), string.Empty);

    public string IconItem
    {
        get => (string)GetValue(ItemMenu.IconItemProperty);
        set => SetValue(ItemMenu.IconItemProperty, value);
    }

    public static readonly BindableProperty NameItemProperty = BindableProperty.Create(nameof(NameItem), typeof(string), typeof(ItemMenu), string.Empty);
    public string NameItem
    {
        get => (string)GetValue(ItemMenu.NameItemProperty);
        set => SetValue(ItemMenu.NameItemProperty, value);
    }

    public static readonly BindableProperty NbItemProperty = BindableProperty.Create(nameof(NbItem), typeof(string), typeof(ItemMenu), string.Empty);

    public string NbItem
    {
        get => (string)GetValue(ItemMenu.NbItemProperty);
        set => SetValue(ItemMenu.NbItemProperty, value);
    }
    public static readonly BindableProperty PageNameProperty = BindableProperty.Create(
    nameof(PageName),
    typeof(string),
    typeof(ItemMenu),
    string.Empty
);

    public string PageName
    {
        get => (string)GetValue(PageNameProperty);
        set => SetValue(PageNameProperty, value);
    }

    public ItemMenu()
	{
		InitializeComponent();
	}



}