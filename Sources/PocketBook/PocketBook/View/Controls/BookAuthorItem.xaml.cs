namespace PocketBook.View.Controls;

public partial class BookAuthorItem : ContentView
{

    public static readonly BindableProperty TextProperty = BindableProperty.Create(
   nameof(Text),
   typeof(string),
   typeof(BookAuthorItem),
   string.Empty
);

    public string Text
    {
        get => (string)GetValue(TextProperty);
        set => SetValue(TextProperty, value);
    }

   
    public BookAuthorItem()
	{
		InitializeComponent();
	}
}