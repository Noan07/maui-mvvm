namespace PocketBook.View.Controls;

public partial class StarsItem : ContentView
{
    public static readonly BindableProperty GradeProperty = BindableProperty.Create(nameof(Grade),typeof(int), typeof(StarsItem));

    public int Grade
    {
        get => (int)GetValue(GradeProperty);
        set => SetValue(GradeProperty, value);
    }
	public StarsItem()
	{
		InitializeComponent();
	}
}