namespace PocketBook.View.Controls;

public partial class FilterBookItem : ContentView
{
	public FilterBookItem()
	{

		InitializeComponent();
	}

    public static readonly BindableProperty NbItemsProperty = BindableProperty.Create(nameof(NbItems), typeof(string), typeof(FilterBookItem));
    public string NbItems
    {
        get => (string)GetValue(FilterBookItem.NbItemsProperty);
        set => SetValue(FilterBookItem.NbItemsProperty, value);
    }

    public static readonly BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(FilterBookItem));

    public string Title
    {
        get => (string)GetValue(FilterBookItem.TitleProperty);
        set => SetValue(FilterBookItem.TitleProperty, value);
    }

}