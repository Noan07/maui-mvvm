namespace PocketBook.View.Controls;

public partial class BookItem : ContentView
{

    public static readonly BindableProperty TitleProperty = BindableProperty.Create(
   nameof(Title),
   typeof(string),
   typeof(BookItem),
   string.Empty
);

    public string Title
    {
        get => (string)GetValue(TitleProperty);
        set => SetValue(TitleProperty, value);
    }

    public static readonly BindableProperty AuthorProperty = BindableProperty.Create(
   nameof(Author),
   typeof(string),
   typeof(BookItem),
   string.Empty
);

    public string Author
    {
        get => (string)GetValue(AuthorProperty);
        set => SetValue(AuthorProperty, value);
    }

    public static readonly BindableProperty StatusProperty = BindableProperty.Create(
   nameof(Status),
   typeof(string),
   typeof(BookItem),
   string.Empty
);

    public string Status
    {
        get => (string)GetValue(StatusProperty);
        set => SetValue(StatusProperty, value);
    }

    public static readonly BindableProperty ImageProperty = BindableProperty.Create(
   nameof(Image),
   typeof(string),
   typeof(BookItem),
   string.Empty
);

    public string Image
    {
        get => (string)GetValue(ImageProperty);
        set => SetValue(ImageProperty, value);
    }

    public static readonly BindableProperty GradeProperty = BindableProperty.Create(
   nameof(Grade),
   typeof(int),
   typeof(BookItem)
);

    public int Grade
    {
        get => (int)GetValue(GradeProperty);
        set => SetValue(GradeProperty, value);
    }


    public BookItem()
	{
		InitializeComponent();
	}
}