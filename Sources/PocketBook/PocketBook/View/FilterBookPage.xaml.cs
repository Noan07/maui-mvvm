using PocketBook.Model;

namespace PocketBook.View;

public partial class FilterBookPage : ContentPage
{
    private List<BookItem> allBooks;

    public FilterBookPage()
    {
        InitializeComponent();

        allBooks = LoadBooks(); 

      
        BooksListView.ItemsSource = allBooks;

    
    }



    private List<BookItem> LoadBooks()
    {
        return new List<BookItem>
            {
                new BookItem { Author = "Test1", Year = 2020 },
                new BookItem { Author = "Test2", Year = 2019 },
                new BookItem { Author = "Test3", Year = 2021 },
                new BookItem { Author = "Test4", Year = 2020 },
            };
    }

    private async void OnCustomBackButtonClicked(object sender, EventArgs e)
    {
        await Navigation.PopAsync();
    }
}