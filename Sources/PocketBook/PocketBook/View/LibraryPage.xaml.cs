using PocketBook.Model;
using PocketBook.ViewModels;
using System.Collections.ObjectModel;


namespace PocketBook.View;

public partial class LibraryPage : ContentPage
{

    public ObservableCollection<LibraryItem> LibraryItemsView { get; set; }
    public ObservableCollection<LibraryItem> LibraryItemsFiltres { get; set; }

    private bool isNavigating = false;
    public LibraryPage()
    {
        InitializeComponent();

        LibraryItemsView = new ObservableCollection<LibraryItem>
        {
            new LibraryItem { NbItem = "45", IconItem = "tray_fill", NameItem = "Tous", PageName = new BooksPage()},
            new LibraryItem { NbItem = "1", IconItem = "person_badge_clock_fill", NameItem = "En pr�t", PageName = new BooksPage()},
            new LibraryItem { NbItem = "", IconItem = "arrow_forward", NameItem = "� lire plus tard", PageName = new BooksPage()},
            new LibraryItem { NbItem = "", IconItem = "eyeglasses", NameItem = "Status de lecture", PageName = new BooksPage()},
            new LibraryItem { NbItem = "", IconItem = "heart_fill", NameItem = "Favoris", PageName = new BooksPage()},
        };

        LibraryItemsFiltres = new ObservableCollection<LibraryItem>
        {
            new LibraryItem { NbItem = "", IconItem = "person_fill", NameItem = "Auteur", PageName = new FilterBookAuteurPage()},
            new LibraryItem { NbItem = "", IconItem = "calendar", NameItem = "Date de publication", PageName = new FilterBookPage()},
            new LibraryItem { NbItem = "", IconItem = "sparkles", NameItem = "Note", PageName = new BooksPage()},
        };

        ItemsCollectionView.ItemsSource = LibraryItemsView;
        ItemsCollectionFiltres.ItemsSource = LibraryItemsFiltres;

    }



    private async void OnItemClicked(object sender, SelectionChangedEventArgs e)
    {
        if (isNavigating)
        {
            return;
        }

        if (e.CurrentSelection.FirstOrDefault() is LibraryItem libraryItem)
        {

            //var detailPage = new BooksPage
            //{
            //    BindingContext = libraryItem
            //};
            await Navigation.PushAsync(libraryItem.PageName);
        }

        ((CollectionView)sender).SelectedItem = null;
        //await Shell.Current.GoToAsync("//books");
    }



}
