using PocketBook.Model;
using System.Collections.ObjectModel;
using System.Linq;

namespace PocketBook.View;

public partial class BooksPage : ContentPage
{

    public BooksPage()
    {
        InitializeComponent();

        var books = new List<BookItem>
            {
                new BookItem { Title = "Livre 1", Author = "Auteur 1", Grade = 2, Status = "Non Termin�e"},
                new BookItem { Title = "Livre 2", Author = "Auteur 2", Grade = 2, Status = "Non Termin�e" },
                new BookItem { Title = "Livre 3", Author = "Auteur 1", Grade = 2, Status = "Non Termin�e" },
                new BookItem { Title = "Livre 4", Author = "Auteur 3", Grade = 2, Status = "Non Termin�e" },

            };

        var sortedBooks = books.OrderBy(book => book.Author);


        var grouped = from book in sortedBooks
                      group book by book.Author into bookGroup
                      select bookGroup;

        GroupedBooks = new ObservableCollection<IGrouping<string, BookItem>>(grouped);

        BooksCollectionView.ItemsSource = GroupedBooks;
    }
    public ObservableCollection<IGrouping<string, BookItem>> GroupedBooks { get; }

    private void OnItemSelected(object sender, SelectionChangedEventArgs e)
    {
        if (e.CurrentSelection.FirstOrDefault() is BookItem selectedBook)
        {
            var detailPage = new BookDetailPage
            {
                BindingContext = selectedBook
            };

            Navigation.PushAsync(detailPage);
        }

    ((CollectionView)sender).SelectedItem = null;
    }
    private async void OnCustomBackButtonClicked(object sender, EventArgs e)
    {
        await Navigation.PopAsync(); 
    }



}