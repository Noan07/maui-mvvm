using PocketBook.Model;

namespace PocketBook.View;

public partial class FilterBookAuteurPage : ContentPage
{


    private List<BookItem> allBooks;

    public FilterBookAuteurPage()
    {
        InitializeComponent();

        allBooks = LoadBooks();


        BooksListView.ItemsSource = allBooks;


    }



    private List<BookItem> LoadBooks()
    {
        return new List<BookItem>
            {
                new BookItem { NbItems = 1, Year = 2020 },
                new BookItem { NbItems = 2, Year = 2019 },
                new BookItem { NbItems = 3, Year = 2021 },
                new BookItem { NbItems = 4, Year = 2020 },
            };
    }

    private async void OnCustomBackButtonClicked(object sender, EventArgs e)
    {
        await Navigation.PopAsync();
    }
}