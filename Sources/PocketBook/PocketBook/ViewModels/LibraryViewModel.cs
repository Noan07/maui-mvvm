﻿using PocketBook.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PocketBook.ViewModels
{
    public class LibraryViewModel
    {
        public ObservableCollection<LibraryItem> Books { get; set; }

        public LibraryViewModel()
        {

            Books = new ObservableCollection<LibraryItem>
            {
                new LibraryItem { NbItem = "rrff", IconItem = "icone1", NameItem = "Livre 1" },
                new LibraryItem { NbItem = "2", IconItem = "icone2", NameItem = "Livre 2" },

            };
        }
    }
}
