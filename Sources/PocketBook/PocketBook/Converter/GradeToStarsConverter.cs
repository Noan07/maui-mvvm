﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Maui.Controls;

namespace PocketBook.Converter
{
    
        public class GradeToStarsConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                if (value is int grade)
                {
                    List<string> stars = new List<string>();

                    for (int i = 0; i < Math.Min(grade, 5); i++)
                    {
                        stars.Add("etoile_fill");
                    }
                    for (int i = grade; i < 5; i++)
                    {
                        stars.Add("etoile");
                    }


                return stars;
                }

                return null;
            }


            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                throw new NotImplementedException();
            }
        }
    

}
