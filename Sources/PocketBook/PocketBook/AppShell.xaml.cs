﻿using PocketBook.View;

namespace PocketBook;

public partial class AppShell : Shell
{
	public AppShell()
	{
		InitializeComponent();
        Routing.RegisterRoute("library", typeof(LibraryPage));
        Routing.RegisterRoute("lists", typeof(ListsPage));
        Routing.RegisterRoute("readings", typeof(ReadingsPage));
        Routing.RegisterRoute("search", typeof(FilterBookPage));
        Routing.RegisterRoute("books", typeof(BooksPage));
    }
}
