# Pocketbook

## Informations Projets
- Application réalisée et testée uniquement avec Android
- Mettre le téléphone en mode claire de préférence pour tester

## Rendus

- Premier rendu (vue):  **(branche master)**
- Deuxième rendu:  **(branche TP2)**
- Troisième rendu **(branche TP3)**: Pas fait finalement
- Quatrième rendu : Pas fait

## Légende

- ❌ Pas fait
- ✅ Fait
- 🔆 Pas écrit à faire dans le TP

## Avancé

### My Library Page

- Bouton + qui affiche le menu: ✅ (Fait mais utilisation pour ajout d'un livre par ISBN sinon problème de clique à travers le ContentView)
- Bouton Modifier: ❌ 🔆
- Affichage du nombre de données par page (uniquement pour Tous): ✅
- Redirection vers BooksPage (Tous) & chargement des données: ✅
- Redirection vers LoanPage & chargement des données: ✅ 
- Redirection vers BooksPage (A lire plus tard) & chargement des données: ✅ 🔆
- Redirection vers BooksPage (Statut de lecture) & chargement des données: ✅ 🔆
- Redirection vers BooksPage (Favoris) & chargement des données: ✅
- Redirection vers FilterPage (Auteur) & chargement des données: ✅
- Redirection vers BooksPage (Date) & chargement des données: ✅
- Redirection vers BooksPage (Note) & chargement des données: ✅ 🔆

### Books Page (Tous)

- Affichage des données: ✅
- Bouton + qui affiche le menu: ✅
- Bouton flèches qui change le trie: ✅
- Utilisation de l'id pour ensuite afficher le livre cliquer: ✅ (Bonus)
- On peut cliquer sur les livres et se faire rediriger: ✅
- Lettres qui changent sur le coté: ❌ (Bonus)
- Pouvoir supprimer un livre (Swipe sur item): ✅
- Pouvoir ajouter aux favoris un livre (Swipe sur item): ✅

### Menu

- Pouvoir afficher le menu: ✅
- Pouvoir ajouter un livre via ISBN: ✅
- Pouvoir scanner un code barre: ❌

### Book Page

- Affichage du titre: ✅
- Affichage de l'auteur: ✅
- Affichage de l'éditeur: ✅
- Affichage de l'image: ✅
- Affichage de la date de publication: ✅
- Affichage du résumé: ❌ (Pas de données)
- Affichage de la note: ✅(Je recois null à chaque fois avec le stub)
- Affichage de la langue: ✅
- Affichage du nombre de pages: ✅
- Affichage de l'ISBN: ✅
- Affichage du statut de lecture: ✅(Je recois inconnue à chaque fois avec le stub)
- Affichage de la date d'ajout à la collection: ❌ 🔆
- Pouvoir changer le statut de lecture: ✅
- Pouvoir prêter le livre: ✅
- Pouvoir ajouter à "A lire plus tard": ✅ 🔆

### Loan Page (mis en place avec la collection de livre et non des livre préter (pas eu le temps))

- Affichage des données: ✅ 
- Pouvoir cliquer sur un livre: ❌ 
- Clique (pret/emprunt): ✅ (mais l'affichage des données est pas bon)
- Pagination: ✅ (mis en place avec la collection de livre et non des livre préter)

### Books Page (A lire plus tard)

- Affichage des données: ✅ 🔆
- Bouton flèches qui change le trie: ✅
- On peut cliquer sur les livres et se faire rediriger: ✅ 🔆

### Books Page (Statut de lecture)

- Affichage des données: ✅ 🔆
- Bouton flèches qui change le trie: ✅
- On peut cliquer sur les livres et se faire rediriger: ✅ 🔆

### Books Page (Favoris)

- Affichage des données: ✅
- Bouton flèches qui change le trie: ✅
- On peut cliquer sur les livres et se faire rediriger: ✅

### Filter Page (Auteur)

- Affichage des auteurs et du nombre de données: ✅
- Bouton flèches qui change le trie: ✅
- Recherche: ✅
- On peut cliquer sur les auteurs et se faire rediriger vers BooksPage: ✅

### Filter Page (Date)

- Affichage des dates et du nombre de données: ✅
- Bouton flèches qui change le trie: ✅
- Recherche: ✅
- On peut cliquer sur les dates et se faire rediriger vers BooksPage: ✅

### Filter Page (Note)

- Affichage des dates et du nombre de données: ✅ 🔆
- Bouton flèches qui change le trie: ✅
- Recherche: ✅
- On peut cliquer sur les dates et se faire rediriger vers BooksPage: ✅ 🔆

### Toolkit

- Utilisation du toolkit personnel: ✅
- Ajout de la RelayCommandAsync: ✅
- Utilisation du community toolkit: ❌